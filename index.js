const { v4: uuidv4 } = require('uuid');

eventContainerify=(item)=>{
    listeners={};
    //attach a subscribe to the class;
    item.subscribe=(type, listener)=>{
        const subscriptionId = uuidv4();
        const subscriptionDate = Date.now();
        listeners[type] = listeners[type]||[];
        listeners[type].push({
            subscriptionDate,
            subscriptionId,
            listener
        });
        return subscriptionId;
    }
    //attach a delete to the class
    item.unsubscribe=(type,subscriptionId)=>{
        if(type in listeners){
            listeners[type].splice(listeners[type].findIndex(x=>x.subscriptionId==subscriptionId),1);
            if(listeners[type].length==0){
                delete(listeners[type]);
            }
        }
    }
    // return the publish (it is up to the caller to protect it)
    return ((eventToPublish)=>{ 
        if (!eventToPublish.type){
            throw new Error("f8b81654-b194-4575-bacd-955123cc7982 - argument exception, type is required property");
        }
        if(eventToPublish.type in listeners){
            listeners[eventToPublish.type].sort((a,b)=>a.subscriptionDate<b.subscriptionDate).forEach((item)=>{
            item.listener(eventToPublish);
            });
        }
    })
}

const createEvent=(type,details)=>{
    correlation = uuidv4();
    eventDate=Date.now();
    return {type,correlation,eventDate,...details}
}
module.exports = {eventContainerify,createEvent};